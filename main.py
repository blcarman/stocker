import requests
from dotenv import load_dotenv
import os
import psycopg
from time import sleep
from datetime import datetime, timedelta
from alpaca.data.timeframe import TimeFrame
from alpaca.data.historical import StockHistoricalDataClient
from alpaca.data.requests import StockLatestQuoteRequest, StockBarsRequest


def init_db():
    # create db if needed
    with psycopg.connect(
        f"host={db_host} port={db_port} user={db_user} password={db_pass}"
    ) as server_conn:
        server_conn.autocommit = True
        databases = server_conn.execute("SELECT datname FROM pg_database;")

        dbs = databases.fetchall()

        if ("stocks",) not in dbs:
            print("creating database")
            cur = server_conn.cursor()
            cur.execute(f"CREATE DATABASE {db_name};")

    # create tables if needed
    with psycopg.connect(connection_string) as db_conn:
        db_cur = db_conn.cursor()

        db_cur.execute(
            "CREATE TABLE IF NOT EXISTS intraday ("
            "time timestamp,"
            "symbol varchar(4),"
            "open_value double precision,"
            "high_value double precision,"
            "low_value double precision,"
            "close_value double precision,"
            "volume integer,"
            "delta double precision,"
            "UNIQUE (time, symbol));"
        )


def get_data(symbol, **kwargs):
    query = f"&apikey={alpha_token}&symbol={symbol}"
    for key, value in kwargs.items():
        query += f"&{key}={value}"
    url = f"https://www.alphavantage.co/query?{query}"
    r = requests.get(url)

    if r.status_code == 200:
        data = r.json()
        return data
    else:
        print("I didn't get any data")
        return []


def insert_data(data):
    with psycopg.connect(connection_string) as db_conn:
        db_cur = db_conn.cursor()

        for item in data:
            db_cur.execute(
                f"INSERT INTO intraday VALUES ('{item.timestamp}', '{item.symbol}', {item.open},  {item.high}, {item.low}, {item.close}, {item.volume}) ON CONFLICT DO NOTHING;"
            )


# get latest date in db

# get full data if symbol doesn't already exist in db

if __name__ == "__main__":
    load_dotenv()

    db_host = os.getenv("DB_HOST")
    db_port = os.getenv("DB_PORT")
    db_user = os.getenv("DB_USER")
    db_pass = os.getenv("DB_PASS")
    stocks = os.getenv("STOCKS").split(",")

    api_key = os.getenv("ALPACA_KEY")
    api_secret = os.getenv("ALPACA_SECRET")
    stock_client = StockHistoricalDataClient(api_key, api_secret)

    db_name = "stocks"

    connection_string = f"host={db_host} port={db_port} user={db_user} password={db_pass} dbname={db_name}"

    init_db()

    # multisymbol_request_params = StockLatestQuoteRequest(symbol_or_symbols=stocks)

    # latest_multisymbol_quotes = stock_client.get_stock_latest_quote(
    #     multisymbol_request_params
    # )

    # print(latest_multisymbol_quotes["SPY"])

    # should we query the db to find out what dates we're missing? might be challenging to add new symbols since we do a single request for all
    # maybe instead of have a separate request for new symbols
    bar_request_params = StockBarsRequest(
        symbol_or_symbols=stocks,
        timeframe=TimeFrame.Day,
        start=datetime.today() - timedelta(days=7),
        end=datetime.today() - timedelta(minutes=30),
    )

    bars = stock_client.get_stock_bars(bar_request_params)

    for stock in stocks:
        insert_data(bars[stock])

# for the number crunching, I guess do the math in python and put it in a new table?
