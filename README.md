# stocker #

## Description ##

This is going to start as a simple stock tracker, and hopefully evolve into being able to make suggestions for trades.

Using postgres for a db. 

```
podman run --name stockdb -e POSTGRES_PASSWORD=Pa55word -d -p 5432  postgres
```

## Roadmap ##

I get overwhelmed with possibilities on this one and end up not doing much of anything. So I think the next plan is start splitting it apart.

* container that gets the data, will be run as a k8s cronjob
* container(s) for number crunching 